var express = require("express");
var app = express();app.listen(3001, () => {
 console.log("Server running on port 3001");

 app.get("/allergies", (req, res, next) => {
    res.json([
        {
          causativeAgent: "peanuts",
          startDate: new Date("2023-05-15"),
          description:
            "Severe allergic reaction with swelling and difficulty breathing.",
        },
        {
          causativeAgent: "pollen",
          startDate: new Date("2022-04-10"),
          description:
            "Seasonal allergy causing sneezing, itching, and watery eyes.",
        },
        {
          causativeAgent: "shellfish",
          startDate: new Date("2021-09-20"),
          description: "Mild allergic reaction with hives and itching.",
        },
      ]);
   });

   app.get('/episodes', (req, res, next) => {
    res.json([
        {
            reason: "fever",
            startDate: new Date('2024-03-01'),
            description: "High fever accompanied by body aches and chills."
        },
        {
            reason: "flu",
            startDate: new Date('2024-02-15'),
            description: "Severe flu symptoms with cough, congestion, and fatigue."
        },
        {
            reason: "injury",
            startDate: new Date('2024-01-10'),
            description: "Ankle sprain resulting from a fall."
        },
      ])
   })
});