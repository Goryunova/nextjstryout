import NextAuth from "next-auth";
import { Provider } from "next-auth/providers/index";

export const uzoAuthOptions = {
  providers: [
    {
      id: "UZO",
      name: "UZO",
      type: "oauth",
      httpOptions: {
        timeout: 10000,
      },
      authorization: {
        params: {
          scope:
            "openid profile myaccount caregiver offline_access WMS IdentityServer:PersistedGrants",
        },
      },
      idToken: true,
      profile(profile: any, tokens: any) {
        console.log(tokens.access_token);

        return fetch("https://account-acc.uwzorgonline.nl/api/profile", {
          method: "GET",
          headers: {
            Authorization: `Bearer ${tokens.access_token}`,
          },
        })
          .then((response: Response) => {
            return response.json();
          })
          .then((data: any) => {
            return {
              id: profile.sub,
              name: `${data.firstName} ${data.lastName}`,
              email: data.email,
            };
          });
      },
      wellKnown:
        "https://identificatie-acc.uwzorgonline.nl/.well-known/openid-configuration",
      clientId: "Portal",
      clientSecret: "837ae7b0-eee4-4f1a-9b6a-b4cdf61f7a5e",
    } as Provider,
  ],
  secret: "v4zYy/ODVVhC/pE3gAW6PNn0eHnNU2K6XmC5iTyisKE=",
  callbacks: {
    async jwt(context: any) {
      const { token, user, account } = context;

      console.log("account tokens");
      console.log(account?.refresh_token);
      console.log(account?.access_token);

      if (account && user) {
        // initial
        return {
          access_token: account?.access_token,
          refresh_token: account?.refresh_token,
          expires_at: account?.expires_at * 1000,
          user,
        };
      }

      // let timeLeft = Math.round((token.expireTime * 1000 - Date.now()) / 1000 / 60);

      // console.log('token time left:', timeLeft);

      // if (timeLeft > 57) {
      //   const timeLeft = (token.expireTime * 1000 - Date.now()) / 1000 / 60;
      //   console.log(
      //     `token is still valid. time left ${Math.round(timeLeft)} minutes left`
      //   );
        return token;
      // }

      // const updatedTokens = await refreshAccessToken(token);

      // console.log('refreshed tokens');
      // console.log(updatedTokens);

      // return updatedTokens;
    },
    async session(context: any) {
      const { session, token } = context;

      if (session && token) {
        session.user = token as any;
      }

      if (token) {
        session.accessToken = token.access_token;
      }

      return session;
    },
  },
};

const handler = NextAuth(uzoAuthOptions);

export { handler as GET, handler as POST };

async function refreshAccessToken(token: any) {
  try {
    console.log(`Token refresh requested. Token: ${token.refreshToken}`);

    const params = new URLSearchParams({
      client_id: "Portal",
      client_secret: "837ae7b0-eee4-4f1a-9b6a-b4cdf61f7a5e",
      grant_type: "refresh_token",
      refresh_token: token.refreshToken,
      scope:
        "openid WMS offline_access IdentityServer:PersistedGrants myaccount",
    });

    const response = await fetch(
      "https://identificatie-acc.uwzorgonline.nl/connect/token",
      {
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
        },
        body: params,
        method: "POST",
      }
    );

    const refreshedTokens = await response.json();

    if (!response.ok) {
      throw refreshedTokens;
    }

    return {
      ...token,
      accessToken: refreshedTokens.access_token,
      expireTime: Date.now() + refreshedTokens.expires_in * 1000,
      refreshToken: refreshedTokens.refresh_token ?? token.refreshToken, // Fall back to old refresh token
    };
  } catch (error) {
    console.log(`Token refresh failed with error:`, error);

    return {
      ...token,
      error: "RefreshAccessTokenError",
    };
  }
}
