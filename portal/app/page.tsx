import getModules, { Module } from "@/lib/user-modules";
import { getServerSession } from "next-auth";
import { uzoAuthOptions } from "./api/auth/[...nextauth]/route";
import { Suspense } from "react";
import Loader from "@/components/loader/loader";
import { getAvailableHeaderRoutes } from "@/utils/modules/modules-routes";
import Link from "next/link";

async function HomeModules() {
  const session = await getServerSession(uzoAuthOptions);

  const modules = await getModules(session.user.access_token);
  const navigation = getAvailableHeaderRoutes(modules);

  return (
    <>
      {navigation && (
        <ul>
          {navigation.map((m: any, index: number) => (
            <Link key={index} href={m.path} className="uzo-card uzo-card-link mb-16">
              <div className="uzo-card-header">
                <h1 className="h4 uzo-card-heading">{m.name}</h1>
              </div>
              <div className="uzo-card-body">
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                  do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                  Ut enim ad minim veniam, quis nostrud exercitation ullamco
                  laboris nisi ut aliquip ex ea commodo consequat.
                </p>
              </div>
            </Link>
          ))}
        </ul>
      )}
    </>
  );
}

export default async function Home() {
  const session = await getServerSession(uzoAuthOptions);

  return (
    <>
      <h1> Welcome, {session.user.user.name}</h1>
      <Suspense fallback={<Loader />}>
        <HomeModules />
      </Suspense>
    </>
  );
}
