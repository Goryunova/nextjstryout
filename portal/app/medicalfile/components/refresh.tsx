'use client'

import Button from "@/components/controls/button";

interface MedicalFileRefreshProps {
    clickCallback: () => void
}

export default function MedicalFileRefresh()  {
    const dataRefresh = () => {
        alert('data will be refreshed');

    }

    return <Button clickCallback={dataRefresh}>Refresh</Button>
}