import { getAllergies, getEpisodes } from "@/lib/medical-file";
import { getServerSession } from "next-auth";
import { uzoAuthOptions } from "../api/auth/[...nextauth]/route";
import { Allergy, Episode } from "@/types/medical-file/medical-file";
import { Suspense } from "react";
import Loader from "@/components/loader/loader";
import Button from "@/components/controls/button";
import MedicalFileRefresh from "./components/refresh";

async function Allergies() {
  const session = await getServerSession(uzoAuthOptions);
  const allergies = await getAllergies(session.user.access_token);

  return (
    <>
      {allergies && (
        <ul>
          {allergies.map((x: Allergy, index: number) => (
            <li key={index}>{x.causativeAgent}</li>
          ))}
        </ul>
      )}
    </>
  );
}

async function Episodes() {
  const session = await getServerSession(uzoAuthOptions);
  /*Question:  I make a call from server component, NextJs caches it and doesn't make this call every time I visit the page. Is it an alternative to React query? */
  /* What is use-case why React query is needed?  */

  const episodes = await getEpisodes(session.user.access_token);

  return (
    <>
      {episodes && (
        <ul>
          {episodes.map((x: Episode, index: number) => (
            <li key={index}>{x.reason}</li>
          ))}
        </ul>
      )}
    </>
  );
}

export default function MedicalFilePage() {
  return (
    <>
      <div className="d-flex align-items-center justify-content-space-between">
        <h1 className="mb-0">Medical file</h1>
        <MedicalFileRefresh />
      </div>
      <div className="d-flex gap-16">
        <div className="uzo-card">
          <h2>Allergies</h2>
          <Suspense fallback={<Loader />}>
            <Allergies />
          </Suspense>
        </div>
        <div className="uzo-card">
          <h2>Episodes</h2>
          <Suspense fallback={<Loader />}>
            <Episodes />
          </Suspense>
        </div>
      </div>
    </>
  );
}

