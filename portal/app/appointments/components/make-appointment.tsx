"use client";

import Agenda from "@/components/agenda/agenda";
import Button from "@/components/controls/button";
import Select from "@/components/form/select";
import Textarea from "@/components/form/textarea";
import Loader from "@/components/loader/loader";
import { getAvailableDates, getAvailableTimeslots } from "@/lib/appointments";
import { Doctor } from "@/types/appointments";
import { useEffect, useState } from "react";

export default function MakeAppointment({ doctors }: { doctors: Doctor[] }) {
  const [selectedDoctorId, setSelectedDoctorId] = useState<string | null>(null);
  const [selectedDay, setSelectedDay] = useState<number | null>(null);
  const [selectedTime, setSelectedTime] = useState<string | null>(null);

  const selectDoctor = (id: string) => {
    setSelectedDoctorId(id);
    setSelectedDay(null);
    setSelectedTime(null);
  };

  const selectDate = (day: number) => {
    setSelectedDay(day);
  };

  const selectTime = (time: string) => {
    setSelectedTime(time);
  };

  return (
    <form className="uzo-form">
      <Doctors doctors={doctors} onSelected={selectDoctor} />
      {selectedDoctorId && (
        <DoctorAgendaDates
          doctorId={selectedDoctorId}
          onSelected={selectDate}
        />
      )}
      {selectedDay && (
        <TimeSlots
          doctorId={selectedDoctorId as string}
          day={selectedDay}
          onTimeSelected={selectTime}
        />
      )}
      {selectedTime && (
        <>
          <Textarea name="reason" id="reason" label="Reason" />
          <div>
            <Button> Create appointment</Button>
          </div>
        </>
      )}
    </form>
  );
}

function Doctors({
  doctors,
  onSelected,
}: {
  doctors: Doctor[];
  onSelected: (id: string) => void;
}) {
  return (
    <Select
      name="doctor"
      id="doctor"
      label="Doctor"
      onSelected={(e) => onSelected(e)}
      options={doctors.map((x) => {
        return { name: x.name, value: x.id };
      })}
    ></Select>
  );
}

function DoctorAgendaDates({
  doctorId,
  onSelected,
}: {
  doctorId: string;
  onSelected: (day: number) => void;
}) {
  const [availableDates, setAvailableDates] = useState<number[]>([]);
  const [loading, setIsLoading] = useState(true);

  useEffect(() => {
    const loadData = async () => {
      setIsLoading(true);
      const data = await getAvailableDates(doctorId);

      setAvailableDates(data);
      setIsLoading(false);
    };

    loadData();
  }, [doctorId]);

  if (loading) return <Loader />;

  return <Agenda availableDates={availableDates} onDaySelected={onSelected} />;
}

function TimeSlots({
  doctorId,
  day,
  onTimeSelected,
}: {
  doctorId: string;
  day: number;
  onTimeSelected: (time: string) => void;
}) {
  const [timeslots, setTimeslots] = useState<string[]>([]);
  const [loading, setIsLoading] = useState(true);

  useEffect(() => {
    setIsLoading(true);
    const getDoctorAgendaTimeSlots = async () => {
      const data = await getAvailableTimeslots(doctorId, day);

      setTimeslots(data);
      setIsLoading(false);
    };

    getDoctorAgendaTimeSlots();
  }, [doctorId, day]);

  if (loading) return <Loader />;

  return (
    <div>
      <h2>Available timeslots</h2>
      <div className="uzo-btn-container flex-row undefined gap-16">
        {timeslots.map((slot) => (
          <Button key={slot} clickCallback={() => onTimeSelected(slot)}>
            {slot}
          </Button>
        ))}
      </div>
    </div>
  );
}
