
import Loader from "@/components/loader/loader";
import { getDoctors } from "@/lib/appointments";
import { Suspense } from "react";
import MakeAppointment from "./components/make-appointment";
import handleFetch from "@/hooks/handle-fetch";

export default async function AppointmentsPage() {
  console.log('AppointmentsPage');
  
  const doctors = await handleFetch(getDoctors);

  return (
    <>
      <h1>Appointments</h1>

      <Suspense fallback={<Loader />}>
        <MakeAppointment doctors={doctors} />
      </Suspense>
    </>
  );
}
