'use client'

import Button from "@/components/controls/button";


export default function NotFound() {
  return (
    <div>
      <h1 className="h1">Route not found</h1>

      <Button clickCallback={() => {console.log('back home')}}>Back to home page</Button>
    </div>
  );
}
