import { NextMiddleware, NextRequest, NextResponse } from "next/server";
import { appMiddlewares } from "./middleware/config";

//https://github.com/nextauthjs/next-auth/issues/7025
export const middleware: NextMiddleware = async (request: NextRequest) => {
  const nextResponse = NextResponse.next();

  const middlewareFunctions = appMiddlewares.map((fn) => fn(request));
  const middlewareHeader = [];

  console.log(middlewareFunctions);

  // Loop through middleware functions
  for (const middleware of middlewareFunctions) {
    // Execute middleware function and await the result
    const resultResponse = await middleware;

    // Check if the result is not okay and return it
    if (!resultResponse.ok) {
      console.log('notok', resultResponse.ok)
      return resultResponse;
    }

    if(resultResponse.headers.get("set-cookie")) {
      console.log('setcookie')
      return resultResponse;
    }

    // Push middleware headers to the array
    middlewareHeader.push(resultResponse.headers);
  }

  //First we are going to define a redirectTo variable
  let redirectTo = null;

  // Check each header in middlewareHeader
  middlewareHeader.some((header) => {
    // Look for the 'x-middleware-request-redirect' header
    const redirect = header.get("x-middleware-request-redirect");

    const setCookie = header.get("set-cookie");

    if(setCookie) {

    }

    console.log('redirect', redirect)

    if (redirect) {
      console.log('redirect!!!', redirect)
      // If a redirect is found, store the value and break the loop
      redirectTo = redirect;
      return true; // Break the loop
    }
    // Continue to the next header in case the redirect header is not found
    return false; // Continue the loop
  });

  // If a redirection is required based on the middleware headers
  if (redirectTo) {
    // Perform the redirection
    return NextResponse.redirect(new URL(redirectTo, req.url), {
      status: 307, // Use the appropriate HTTP status code for the redirect
    });
  }

  // If no redirection is needed, proceed to the next middleware or route handler
  return nextResponse;
};

export const config = {
  matcher: ["/((?!api|_next/static|_next/image|favicon.ico).*)"],
};
