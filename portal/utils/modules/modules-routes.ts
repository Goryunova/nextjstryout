import { Module } from "@/lib/user-modules";

export enum MODULES  {
    HHR = 0,
    MESSAGECENTER,
    MEDICALFILE,
    SELFMEASUREMENTS,
    WEBAGENDA
};


const MODULE_IDENTIFIERS: {[key: string] : MODULES} = {
    '1': MODULES.HHR,
    '2': MODULES.MEDICALFILE, 
    '3': MODULES.SELFMEASUREMENTS,
    '4': MODULES.WEBAGENDA
}

const HEADER_ROUTES = [
    {name: 'Medical file', path: '/medicalfile', require: [MODULES.MEDICALFILE]}, 
    {name: 'Medications', path: '/mco', require: [MODULES.HHR]}, 
    {name: 'Self Measurements', path: '/measurements', require: [MODULES.SELFMEASUREMENTS]},
    {name: 'Appointments', path: '/appointments', require: [MODULES.WEBAGENDA]}
]
export function getAvailableHeaderRoutes(modules: Module[]): any {
    const availableModules = modules.map((m: Module) => (MODULE_IDENTIFIERS)[m.moduleId]);

    const routes = HEADER_ROUTES.filter((r: any) => r.require.every((am: MODULES) => availableModules.indexOf(am) != -1));

    return routes;
}