"use client";

import Link from "next/link";
import { usePathname } from "next/navigation";

type NavigationProps = {
  routes: any[];
};

export default function Navigation({ routes }: NavigationProps) {
  const currentPath = usePathname();

  const renderNavigationItem = (
    label: string,
    path: string,
    isActive: (path: string) => boolean, 
    index: number
  ) => {
    return (
      <li key={index}
        className={`uzo-main-navigation-item ${
          isActive(path) ? "uzo-main-navigation-item-active" : ""
        }`}
      >
        <Link
          className="uzo-main-navigation-link"
          aria-current={isActive(path) ? "page" : "false"}
          href={path}
        >
          {label}
        </Link>
      </li>
    );
  };

  return (
    <>
      {renderNavigationItem(
        "Home",
        "/",
        (path: string) => currentPath === path, 0
      )}
      {routes &&
        routes.map((m: any, index: number) =>
          renderNavigationItem(m.name, m.path, (path: string) =>
            currentPath.includes(path), index
          )
        )}
    </>
  );
}
