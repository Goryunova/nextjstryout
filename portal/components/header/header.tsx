import getModules from "@/lib/user-modules";
import Navigation from "./navigation/navigation";
import { getAvailableHeaderRoutes } from "@/utils/modules/modules-routes";
import handleFetch from "@/hooks/handle-fetch";

export default async function Header() {
  const modules = await handleFetch(getModules);

  const navigation = getAvailableHeaderRoutes(modules);

  return (
    <header className="uzo-header">
      <div className="uzo-header-skiplink">
        <div className="innerWrapper">
          <span className="skipLinkHeading">Ga direct naar:</span>
          <ul className="skipLinkList">
            <li className="skipLinkListItem">
              <a href="#menu" className="uzo-btn uzo-btn-link">
                Hoofdmenu
              </a>
            </li>
          </ul>
        </div>
      </div>

      <div className="uzo-header-top lightColor">
        <div className="innerWrapper">
          <div className="uzo-header-top-title">Huisartsenpraktijk Thoolen</div>
          <div className="uzo-btn-action-list uzo-btn-action-list-connected uzo-header-practice">
            <button
              className="uzo-btn uzo-btn-white-link"
              aria-expanded="false"
            >
              <span>Switch to other practice</span>
              <i
                className="uzo-icon uzo-icon-angle-down"
                aria-hidden="true"
              ></i>
            </button>
          </div>
        </div>
      </div>
      <div className="uzo-header-bottom">
        <div className="innerWrapper">
          <div className="uzo-header-logo">
            <a className="uzo-header-brand-logo-link" href="/">
              Logo
            </a>
          </div>

          <div className="uzo-header-menu">
            <nav
              className="uzo-main-navigation"
              id="menu"
              aria-labelledby="mainNavigationHeading"
            >
              <h2 id="mainNavigationHeading" className="sr-only">
                Hoofdmenu
              </h2>
              <ul className="uzo-main-navigation-list">
                <Navigation routes={navigation} />
              </ul>
            </nav>
          </div>

          <div className="uzo-header-account">
            <ul className="uzo-header-account-list">
              <li className="uzo-header-account-list-item uzo-header-practice">
                <div className="uzo-btn-action-list uzo-btn-action-list-connected">
                  <button
                    className="uzo-btn uzo-btn-black-link"
                    aria-expanded="false"
                  >
                    <i
                      className="uzo-icon uzo-icon-building-user"
                      aria-hidden="true"
                    ></i>
                    <span className="sr-only">Switch to other practice</span>
                  </button>
                </div>
              </li>
              <li className="uzo-header-account-list-item uzo-header-account-sign-out">
                <button className="uzo-btn uzo-btn-black-link">
                  <i
                    className="uzo-icon uzo-icon-arrow-right-from-bracket"
                    aria-hidden="true"
                  ></i>
                  <span>Sign out</span>
                </button>
              </li>
              <li className="uzo-header-account-list-item">
                <div className="uzo-btn-action-list uzo-btn-action-list-connected">
                  <button
                    className="uzo-btn uzo-btn-black-link uzo-header-account-profile"
                    aria-expanded="false"
                  >
                    <i
                      className="uzo-icon uzo-icon-user uzo-icon-header-account"
                      aria-hidden="true"
                    ></i>
                    {/* <span className="sr-only-max-sm">{session?.user?.user?.name}</span> */}
                  </button>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </header>
  );
}
