import { useState } from "react";

export default function Agenda({
  availableDates,
  onDaySelected,
}: {
  availableDates: number[];
  onDaySelected: (day: number) => void;
}) {
  const [selectedDay, setSelectedDay] = useState<number | null>(null);

  const onDateSelected = (day: number) => {
    if (availableDates?.indexOf(day) === -1) return;

    onDaySelected(day);
    setSelectedDay(day);
  };

  const renderDataBlock = (dates: number[]) => {
    return dates.map((x: number) => {
      const dayClass = `uzo-agenda-day ${
        availableDates?.indexOf(x) === -1 ? "uzo-agenda-day-unavailable" : ""
      } ${x === selectedDay ? "uzo-agenda-day-selected" : ""}`;

      return (
        <td key={x}>
          <div onClick={() => onDateSelected(x)} className={dayClass}>
            {x}
            <span className="sr-only">
              februari 2024 maandag, niet beschikbaar
            </span>
          </div>
        </td>
      );
    });
  };

  return (
    <div className="uzo-agenda">
      <div className="uzo-agenda-heading">
        <button className="uzo-btn uzo-btn-link">
          <span className="sr-only">Vorige maand</span>
          <i className="uzo-icon uzo-icon-angle-left" aria-hidden="true"></i>
        </button>
        <div className="uzo-agenda-heading-current-month">
          <span className="sr-only">maand: februari, jaar: 2024</span>
          <span aria-hidden="true">februari 2024</span>
        </div>
        <button className="uzo-btn uzo-btn-link">
          <span className="sr-only">Volgende maand</span>
          <i className="uzo-icon uzo-icon-angle-right" aria-hidden="true"></i>
        </button>
      </div>

      <table className="uzo-agenda-table">
        <thead>
          <tr>
            <th>Ma</th>
            <th>Di</th>
            <th>Wo</th>
            <th>Do</th>
            <th>Vr</th>
            <th>Za</th>
            <th>Zo</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              <div className="uzo-agenda-day uzo-agenda-day-other-month">
                29
              </div>
            </td>
            <td>
              <div className="uzo-agenda-day uzo-agenda-day-other-month">
                30
              </div>
            </td>
            <td>
              <div className="uzo-agenda-day uzo-agenda-day-other-month">
                31
              </div>
            </td>
            {renderDataBlock([1, 2, 3, 4])}
          </tr>
          <tr>{renderDataBlock([5, 6, 7, 8, 9, 10, 11])}</tr>
          <tr>{renderDataBlock([12, 13, 14, 15, 16, 17, 18])}</tr>
          <tr>{renderDataBlock([19, 20, 21, 22, 23, 24, 25])}</tr>
          <tr>
            {renderDataBlock([26, 27, 28, 29])}

            <td>
              <div className="uzo-agenda-day uzo-agenda-day-other-month">1</div>
            </td>
            <td>
              <div className="uzo-agenda-day uzo-agenda-day-other-month">2</div>
            </td>
            <td>
              <div className="uzo-agenda-day uzo-agenda-day-other-month">3</div>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  );
}
