import { InputProps } from "./props";

export default function Textarea(props: InputProps) {
  return (
    <div className="uzo-element-wrapper ">
      <label htmlFor={props.id} className="uzo-form-label">
        {props.label}
      </label>
      <textarea name={props.name} id={props.id} className="uzo-form-control"></textarea>
    </div>
  );
}
