import { InputProps } from "./props";

export interface SelectOption {
  name: string;
  value: string;
}

interface SelectProps extends InputProps {
  options: SelectOption[];
  onSelected: (value: string) => void;
}

export default function Select(props: SelectProps) {
  return (
    <div className="uzo-element-wrapper">
      <label htmlFor={props.name} className="uzo-form-label">
        {props.label}
      </label>
      <select
        onChange={(e) => props.onSelected(e.target.value)}
        id="country"
        name={props.name}
        className="uzo-form-select"
        aria-describedby="country-validation"
        defaultValue=""
      >
        <option disabled value="">
          Selecteer
        </option>
        {props.options.map((opt) => (
          <option key={opt.value} value={opt.value}>
            {opt.name}
          </option>
        ))}
      </select>
      {/* <div className="uzo-form-validation" id="country-validation">
        <span className="uzo-form-validation-message">
          Het veld 'Land' is niet ingevuld.
        </span>
      </div> */}
    </div>
  );
}
