import { InputProps } from "./props";

export default function Input(props: InputProps) {
  return (
    <div className="uzo-element-wrapper">
      <label htmlFor={props.id} className="uzo-form-label">
        {props.label}
      </label>
      <input name={props.name} id={props.id} className="uzo-form-control" />
    </div>
  );
}
