export interface InputProps {
    name: string;
    label: string;
    id: string;
}