interface ButtonProps {
    clickCallback?: () => void;
    children: any;
}

export default function Button(props: ButtonProps) {
  return (
    <button
      onClick={props.clickCallback}
      className="uzo-btn uzo-btn-primary"
      type="button"
    >
        {
            props.children
        }
    </button>
  );
}
