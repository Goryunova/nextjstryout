export default function Loader() {
  return (
    <div className="container">
      <div className="sr-only">A loading text goes here</div>
      <div
        className="d-flex flex-column gap-16 font-size-12"
        aria-hidden="true"
      >
        <div className="uzo-skeleton uzo-heading-skeleton px-16 w-50"></div>
        <div className="d-flex flex-column gap-8">
          <div className="uzo-skeleton uzo-p-skeleton px-16"></div>
          <div className="uzo-skeleton uzo-p-skeleton px-16"></div>
          <div className="uzo-skeleton uzo-p-skeleton px-16 w-75"></div>
        </div>
        <div className="uzo-skeleton uzo-btn-skeleton px-16 w-25"></div>
      </div>
    </div>
  );
}
