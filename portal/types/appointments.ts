export type Doctor = {
    id: string;
    name: string;
}
