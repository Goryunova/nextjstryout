export type Allergy = {
    causativeAgent: string;
    startDate: Date;
    description: string;
}

export type Episode = {
    reason: string;
    startDate: Date;
    description: string;
}