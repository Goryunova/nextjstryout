export async function getAllergies(token: string) {
  if (token == null) {
    throw new Error("Token must be provided to make this call");
  }

  const res = await fetch('http://localhost:3001/allergies');

  return res.json();
}


export async function getEpisodes(token: string) {
    if (token == null) {
      throw new Error("Token must be provided to make this call");
    }


    const t = new Promise((resolve, reject) => {
      const timer = setTimeout(async () => {
        const res = await fetch('http://localhost:3001/episodes');
  
        const data = await res.json();

        resolve(data);
      }, 40000)
    })


    return t;
  }
  