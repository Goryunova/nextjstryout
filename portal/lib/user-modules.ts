export type Module = {
  moduleId: string;
  status: number;
}

export enum ModuleAvailabilityStatus {
  full = 0, 
  readonly, 
  age_restriction
}

export default async function getModules(token: string): Promise<Module[]> {
  if (token == null) {
    throw new Error('Token must be provided to make this call');
  }

  const data = new Promise((resolve: (value: Module[]) => void) =>
    setTimeout(
      () =>
        resolve([
          { moduleId: "2", status: ModuleAvailabilityStatus.full },
          { moduleId: "1", status: ModuleAvailabilityStatus.full },
          { moduleId: "3", status: ModuleAvailabilityStatus.readonly },
          { moduleId: "4", status: ModuleAvailabilityStatus.full },
        ]),
      5000
    )
  );

  return data;
}
