import { Doctor } from "@/types/appointments";

export async function getDoctors(token: string): Promise<Doctor[]> {
  const data =  [
    { id: "doc001", name: "Dr. Smith" },
    { id: "doc002", name: "Dr. Johnson" },
    { id: "doc003", name: "Dr. Williams" },
    { id: "doc004", name: "Dr. Brown" },
    { id: "doc005", name: "Dr. Jones" },
  ];

  return new Promise((resolve) => setTimeout(() => resolve(data), 2000));
}

export async function getAvailableDates(doctorId: string): Promise<number[]> {
  if (doctorId == null) {
    throw new Error('Doctor must not be null');
  }

  let data: number[] = [];

  switch(doctorId) {
    case "doc001": 
      data = [1, 2, 3,4, 5, 7, 15, 16, 17, 18, 21];
      break;
    case "doc005": 
    case "doc002": 
      data = [1, 2, 5, 7, 15, 21, 27];
      break;
    default:
      data = [1, 2, 10, 21, 26];
  }

  return new Promise((resolve) => setTimeout(() => resolve(data), 2000));
}


export function getAvailableTimeslots(doctorId: string, day: number): Promise<string[]> {
  if (doctorId == null || day == null) {
    throw new Error('Doctor and day must not be null');
  }

  let data: string[] = [];

  switch(doctorId) {
    case "doc001": 
      data = ['14:50', '15:40', '08:30', '12:15', '17:20', '10:45', '13:55', '19:10', '21:30', '16:25'];
      break;
    case "doc005": 
    case "doc002": 
      data = ['09:25', '13:10', '16:55', '11:20', '18:30', '20:45', '22:15', '17:05'];
      break;
    default:
      data = [ '11:30', '16:20', '12:45', '13:55', '18:10', '19:30', '17:25'];

  }

  return new Promise((resolve) => setTimeout(() => resolve(data), 2000));
}
