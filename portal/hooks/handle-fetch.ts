import { uzoAuthOptions } from "@/app/api/auth/[...nextauth]/route";
import { getServerSession } from "next-auth";

export default async function handleFetch(originalFunction: (token: string) => Promise<any>) {
    const session = await getServerSession(uzoAuthOptions);

    return originalFunction(session.user.access_token);
}