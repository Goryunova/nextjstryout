import { encode, getToken } from "next-auth/jwt";
import { NextRequest, NextResponse } from "next/server";

export default async function AuthMiddleware (request: NextRequest) {
    const token = await getToken({ req: request });

    let response = NextResponse.next();
  
    if (!token) {
      return NextResponse.redirect(new URL('/api/auth/signin', request.url));
  }
  
    const needToRefresh = needTokenRefresh(token);
  
    if (needToRefresh) {
      console.log("start token refresh");
  
      const newTokens = await refreshAccessToken(token);
  
      console.log("token refresh done");
      console.log(newTokens);
  
      const newSessionToken = await encode({
        secret: "v4zYy/ODVVhC/pE3gAW6PNn0eHnNU2K6XmC5iTyisKE=",
        token: newTokens,
        maxAge: 60 * 60,
      });
  
      console.log('newSessionToken');
      console.log(newSessionToken);
      response = updateCookie(newSessionToken, request, response);
    }
  
    return response;
}

const needTokenRefresh = (token: any) => {
    let timeLeft = Math.round((token.expires_at - Date.now()) / 1000 / 60);
  
    console.log(`token time left: ${timeLeft}, expires at: ${token.expires_at}`);
  
    return timeLeft < 1;
  };
  
  async function refreshAccessToken(token: any) {
    try {
      console.log(`Token refresh requested. Token: ${token.refresh_token}`);
  
      const params = new URLSearchParams({
        client_id: "Portal",
        client_secret: "837ae7b0-eee4-4f1a-9b6a-b4cdf61f7a5e",
        grant_type: "refresh_token",
        refresh_token: token.refresh_token,
        scope:
          "openid WMS offline_access IdentityServer:PersistedGrants myaccount",
      });
  
      const response = await fetch(
        "https://identificatie-test.uwzorgonline.nl/connect/token",
        {
          headers: {
            "Content-Type": "application/x-www-form-urlencoded",
          },
          body: params,
          method: "POST",
        }
      );
  
      const refreshedTokens = await response.json();
  
      console.log(refreshedTokens);
  
      if (!response.ok) {
        throw refreshedTokens;
      }
  
      delete token.error;
  
      return {
        ...token,
        access_token: refreshedTokens.access_token,
        expires_at: Date.now() + (refreshedTokens.expires_in * 1000),
        refresh_token: refreshedTokens.refresh_token ?? token.refresh_token, // Fall back to old refresh token
      };
    } catch (error) {
      console.log(`Token refresh failed with error:`, error);
  
      return {
        ...token,
        error: "RefreshAccessTokenError",
      };
    }
  }
  
  export function updateCookie(
    sessionToken: string | null,
    request: NextRequest,
    response: NextResponse
  ): NextResponse<unknown> {
    /*
     * BASIC IDEA:
     *
     * 1. Set request cookies for the incoming getServerSession to read new session
     * 2. Updated request cookie can only be passed to server if it's passed down here after setting its updates
     * 3. Set response cookies to send back to browser
     */
  
    console.log('update cookie');
    if (sessionToken) {
      console.log('set cookie token' , sessionToken)
      // Set the session token in the request and response cookies for a valid session
      request.cookies.set('next-auth.session-token', sessionToken);
      response = NextResponse.next({
        request: {
          headers: request.headers,
        },
      });
      response.cookies.set('next-auth.session-token', sessionToken, {
        httpOnly: true,
        maxAge: 60 * 60,
        secure: true,
        sameSite: "lax",
      });
    } else {
      request.cookies.delete('next-auth.session-token');
  
      return NextResponse.redirect(new URL('/api/auth/signin', request.url));
    }
  
    return response;
  }